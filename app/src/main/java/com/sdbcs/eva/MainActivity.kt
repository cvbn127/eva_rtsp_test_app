package com.sdbcs.eva

import android.Manifest
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.graphics.SurfaceTexture
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.TextureView
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat


class MainActivity : AppCompatActivity() {
    private lateinit var streamButton: Button
    private lateinit var previewTexture: AutoFitTextureView

    private var rtspService: EvaService? = null
    private var isServiceBound = false
    private var isPreviewTextureInited = false

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, iBinder: IBinder) {
            Log.d(TAG, "ServiceConnection: connected to service.")
            // We've bound to MyService, cast the IBinder and get MyBinder instance
            val binder = iBinder as EvaService.EvaServiceBinder
            rtspService = binder.service
            isServiceBound = true
            onTextureAvailable()
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            Log.d(TAG, "ServiceConnection: disconnected from service.")
            isServiceBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        streamButton = findViewById(R.id.streamButton)
        streamButton.setOnClickListener {
            if (isPermissionsGranted()) {
                if (isServiceBound) {
                    if (rtspService?.isStreamStarted() == false) {
                        startStream()
                    } else {
                        stopStream()
                    }
                } else {
                    startService()
                    startStream()
                }
            } else {
                requestPermissions()
            }
        }
        previewTexture = findViewById(R.id.previewTexture)

        previewTexture.surfaceTextureListener = object : TextureView.SurfaceTextureListener {
            override fun onSurfaceTextureAvailable(
                    surface: SurfaceTexture?,
                    width: Int,
                    height: Int
            ) {
                isPreviewTextureInited = true
                onTextureAvailable()
            }

            override fun onSurfaceTextureSizeChanged(
                    surface: SurfaceTexture?,
                    width: Int,
                    height: Int
            ) {
                return Unit
            }

            override fun onSurfaceTextureUpdated(surface: SurfaceTexture?) {
                return Unit
            }

            override fun onSurfaceTextureDestroyed(surface: SurfaceTexture?): Boolean {
                isPreviewTextureInited = false
                rtspService?.onPreviewTextureDestroyed()
                return true
            }
        }

    }

    private fun startService() {
        if (!isServiceBound) {
            Intent(this, EvaService::class.java).also { intent ->
                startService(intent)
            }
        }
    }

    private fun stopService() {
        Intent(this, EvaService::class.java).also { intent ->
            unbindService(serviceConnection)
        }
        Intent(this, EvaService::class.java).also { intent ->
            stopService(intent)
        }
    }

    private fun startStream() {
        rtspService?.startStream()
        streamButton.text = getString(R.string.stream_stop_text)
    }

    private fun stopStream() {
        rtspService?.stopStream()
        streamButton.text = getString(R.string.stream_start_text)
    }

    private fun onTextureAvailable() {
        if (isPreviewTextureInited && isServiceBound) {
            rtspService?.onPreviewTextureAvailable(previewTexture)

            if (rtspService?.isStreamStarted() == true) {
                streamButton.text = getString(R.string.stream_stop_text)
            }
        }
    }

    override fun onResume() {
        if (!isPermissionsGranted()) {
            requestPermissions()
        } else {
            Intent(this, EvaService::class.java).also { intent ->
                bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
            }
        }
        super.onResume()
    }

    override fun onDestroy() {
        if (rtspService == null || rtspService?.isStreamStarted() == false) {
            stopService()
        }
        super.onDestroy()
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)
    }

    private fun isPermissionsGranted(): Boolean {
        return REQUIRED_PERMISSIONS.all {
            ContextCompat.checkSelfPermission(baseContext, it) == PackageManager.PERMISSION_GRANTED
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE_PERMISSIONS -> {
                if (!isPermissionsGranted()) {
                    Log.e(TAG, "permissions not granted!")
                    finish()
                } else {
                    startService()
                }
            }
        }
    }

    companion object {

        private const val TAG = "EVA_MAIN_ACTIVITY"

        private val REQUIRED_PERMISSIONS = arrayOf(
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.INTERNET,
                Manifest.permission.FOREGROUND_SERVICE
        )
        private const val REQUEST_CODE_PERMISSIONS = 42
    }
}