package com.sdbcs.eva

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.ImageFormat
import android.hardware.camera2.CameraManager
import android.media.Image
import android.media.ImageReader
import android.os.Binder
import android.os.Handler
import android.os.HandlerThread
import android.os.IBinder
import android.util.Log
import android.view.Surface
import androidx.core.app.NotificationCompat

import com.sdbcs.eva.rtsplib.RtspService
import java.nio.ByteBuffer

class EvaService : Service() {

    inner class EvaServiceBinder : Binder() {
        val service: EvaService
            get() = this@EvaService
    }

    private var backgroundThread: HandlerThread? = null
    private var backgroundHandler: Handler? = null

    private val binder: EvaServiceBinder = EvaServiceBinder()
    private lateinit var cameraHelper: CameraHelper
    private var previewTexture: AutoFitTextureView? = null

    private var imageReader: ImageReader? = null
    private var imageReaderSurface: Surface? = null

    private lateinit var notificationManager: NotificationManager
    private lateinit var notificationChannel: NotificationChannel

    private var rtspService: RtspService? = null

    private var imageWidth = 640
    private var imageHeight = 480

    private val rtspServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, iBinder: IBinder) {
            Log.d(TAG, "ServiceConnection: connected to service.")
            val binder = iBinder as RtspService.RtspServiceBinder
            rtspService = binder.service
            onRtspServiceBound()
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            Log.d(TAG, "ServiceConnection: disconnected from service.")
        }
    }

    private fun onRtspServiceBound() {
        if (rtspService == null) {
            Log.e(TAG, "rtsp service not bound yet")
            return
        }
        initedDevices.rtspService = true
        onDevicesInited()
    }

    private fun startBackgroundThread(): Boolean {
        backgroundThread = HandlerThread("EvaBackgroundThread").also { it.start() }
        if (backgroundThread?.isAlive == false) {
            return false
        }

        @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
        backgroundHandler = Handler(backgroundThread?.looper)
        return true
    }

    fun startStream() {
        onDevicesInited()
        rtspService?.startStream()
    }

    fun isStreamStarted(): Boolean {
        if (rtspService == null) {
            return false
        } else {
            return rtspService?.isStreamStarted()!!
        }
    }

    fun stopStream() {
        rtspService?.pauseStream()
    }

    private fun stopBackgroundThread() {
        backgroundThread?.quitSafely()
        try {
            backgroundThread?.join()
            backgroundThread = null
            backgroundHandler = null
        } catch (e: InterruptedException) {
            Log.e(TAG, e.toString())
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    private fun startNotification() {
        notificationChannel =
            NotificationChannel(
                CHANNEL_ID,
                NOTIFICATION_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT
            )

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(notificationChannel)
        val notification: Notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("A service is running in the background")
            .setContentText("Streaming is ${isStreamStarted()}").build()

        startForeground(42, notification)

    }

    override fun onCreate() {

        Intent(this, RtspService::class.java).also { intent ->
            startService(intent)
        }
        Intent(this, RtspService::class.java).also { intent ->
            bindService(intent, rtspServiceConnection, Context.BIND_AUTO_CREATE)
        }

        imageReader =
            ImageReader.newInstance(
                imageWidth,
                imageHeight,
                ImageFormat.YUV_420_888,
                50
            ) //fps * 10 min
        imageReader?.setOnImageAvailableListener(onImageAvailableListener, null)
        imageReaderSurface = imageReader?.surface

        startBackgroundThread()

        val cameraManager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        cameraHelper = CameraHelper(cameraManager, backgroundHandler!!)

        cameraHelper.initCamera()
        cameraHelper.onOpenedCallback = {
            Log.i(TAG, "on camera open!")
            initedDevices.camera = true
            onDevicesInited()
        }
        cameraHelper.tryOpenCamera()

        startNotification()

        super.onCreate()
    }

    override fun onDestroy() {
        rtspService?.stopSelf()
        stopBackgroundThread()
        super.onDestroy()
    }

    fun onPreviewTextureAvailable(preview: AutoFitTextureView) {
        previewTexture = preview
        initedDevices.previewSurface = true
        onDevicesInited()
    }

    fun onPreviewTextureDestroyed() {
        previewTexture = null
    }

    companion object {
        val TAG = "EvaService"
        val CHANNEL_ID = "Eva Rtsp service channel"
        val NOTIFICATION_CHANNEL_NAME = "Eva Notification Channel"
    }

    private val onImageAvailableListener = ImageReader.OnImageAvailableListener { reader ->
        if (reader != null) {
            val img = reader.acquireLatestImage()
            img?.let {

                val imgArray = ByteArray(it.planes[0].buffer.remaining())
                it.planes[0].buffer.get(imgArray)

                val imgArray1 = ByteArray(it.planes[1].buffer.remaining())
                it.planes[1].buffer.get(imgArray1)

                val imgArray2 = ByteArray(it.planes[2].buffer.remaining())
                it.planes[2].buffer.get(imgArray2)

                val a = imgArray + imgArray1 /*+ imgArray2*/
                rtspService?.feed(a)

                it.close()
            }
        }
    }

    private fun onDevicesInited() {
        if (!initedDevices.isInited()) {
            return
        }
        val surfaceTexture = previewTexture?.surfaceTexture
        surfaceTexture?.setDefaultBufferSize(imageWidth, imageHeight)

        if (surfaceTexture != null) {
            val surface = Surface(surfaceTexture)
            var res = cameraHelper.createCameraPreviewSession(
                mutableListOf(
                    surface,
                    imageReaderSurface!!
                )
            )
        }
    }

    private var initedDevices = object {
        var camera: Boolean = false
        var previewSurface: Boolean = false
        var rtspService: Boolean = false

        fun isInited(): Boolean {
            return camera && previewSurface && rtspService
        }
    }
}