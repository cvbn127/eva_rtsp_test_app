package com.sdbcs.eva

import android.annotation.SuppressLint
import android.hardware.camera2.*
import android.os.Handler
import android.util.Log
import android.util.SparseIntArray
import android.view.Surface

class CameraHelper(private var cameraManager: CameraManager, private var handler: Handler) {

    private var mainCamera: CameraDevice? = null
    private lateinit var mainCameraId: String
    private var cameraCaptureSession: CameraCaptureSession? = null

    var onOpenedCallback: () -> Unit = {}

    fun initCamera(): Boolean {
        Log.i(TAG, "initCamera()")
        val cameraIdList = cameraManager.cameraIdList
        for (cameraId in cameraIdList) {
            val characteristics = cameraManager.getCameraCharacteristics(cameraId)
            val facing = characteristics.get(CameraCharacteristics.LENS_FACING)
            if (facing == CameraCharacteristics.LENS_FACING_BACK) {
                mainCameraId = cameraId
                return true
            }
        }
        Log.e(TAG, "Camera with back facing lens not found")
        return false
    }

    @SuppressLint("MissingPermission")
    fun tryOpenCamera() {
        if (isOpen()) {
            return
        }
        close()
        try {
            cameraManager.openCamera(mainCameraId, object : CameraDevice.StateCallback() {
                override fun onOpened(camera: CameraDevice) {
                    mainCamera = camera
                    Log.i(TAG, "Camera with id: ${mainCameraId} has been opened")
                    onOpenedCallback()
                }

                override fun onDisconnected(camera: CameraDevice) {
                    camera.close()
                    mainCamera = null
                }

                override fun onError(camera: CameraDevice, error: Int) {
                    onDisconnected(camera)
                }
            }, handler)
        } catch (e: CameraAccessException) {
            Log.e(TAG, e.toString())
        }

    }

    fun isOpen(): Boolean {
        return mainCamera != null
    }

    fun close() {
        try {
            cameraCaptureSession?.stopRepeating()
            cameraCaptureSession?.abortCaptures()
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }

        cameraCaptureSession?.close()
        cameraCaptureSession = null
        mainCamera?.close()
        mainCamera = null
    }

    fun getOrientation(): Int {
        val characteristics = cameraManager.getCameraCharacteristics(mainCameraId)
        val result = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION) ?: 0
        return result
    }

    fun createCameraPreviewSession(surfaceList: MutableList<Surface>): Boolean {
        try {
            val captureRequestBuilder = mainCamera?.createCaptureRequest(CameraDevice.TEMPLATE_RECORD)
            for (surface in surfaceList) {
                captureRequestBuilder?.addTarget(surface)
            }

            mainCamera?.createCaptureSession(surfaceList, object : CameraCaptureSession.StateCallback() {
                override fun onConfigured(session: CameraCaptureSession) {
                    cameraCaptureSession = session
                    try {
                        cameraCaptureSession?.setRepeatingRequest(captureRequestBuilder?.build()!!, null, handler)
                    } catch (e: CameraAccessException) {
                        e.printStackTrace()
                    }
                }

                override fun onConfigureFailed(session: CameraCaptureSession) {
                    Log.e(TAG, "Camera ${mainCameraId}. Configuration capture session failed!")
                }
            }, handler)
            return true
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
        return false
    }

    companion object {
        const val TAG = "CAMERA_SERVICE"

        /**
         * Conversion from screen rotation to JPEG orientation.
         */
        private val ORIENTATIONS = SparseIntArray()

        init {
            ORIENTATIONS.append(Surface.ROTATION_0, 90)
            ORIENTATIONS.append(Surface.ROTATION_90, 0)
            ORIENTATIONS.append(Surface.ROTATION_180, 270)
            ORIENTATIONS.append(Surface.ROTATION_270, 180)
        }
    }
}